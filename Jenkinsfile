pipeline {
    agent any
    
    parameters {
        string(name: 'Environment', defaultValue: 'develop', description: 'Environment/Branch name')
    }
    
    environment {
        REGISTRY_NAME = 'myacrregistry'
        REGISTRY = 'myacrregistry.azurecr.io/'
        GIT_REPO_URL = 'https://github.com/afaniuolo/docker-examples.git'
        COMPOSE_PROJECT_NAME = "mycustomsolution"
        PRINCIPAL_SERVICE_CREDENTIAL_ID = "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
        AKS_RESOURCE_GROUP = 'sc10'
        AKS_CLUSTER_NAME = 'sc10cluster'
        AKS_NAMESPACE = 'sitecore'
        KUBERNETES_CREDENTIALS_ID = 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'
        AKS_CLUSTER_API_URL = 'https://sc10cluste-sc10-xxxxxx-xxxxxxxx.hcp.eastus.azmk8s.io:443'
        VERSION = ''
    }

    stages {
        stage('Checkout') {
            steps {
                checkout([$class: 'GitSCM', branches: [[name: '*/' + params.Environment]], extensions: [], userRemoteConfigs: [[url: GIT_REPO_URL]]])
            }
        }
        
        stage('Find Version of Latest Built Images')
        {
            steps {
                script {
                    
                    azureCLI commands: [[exportVariablesString: '', script: 'az acr login --name ' + REGISTRY]], principalCredentialId: PRINCIPAL_SERVICE_CREDENTIAL_ID
                    def latest = powershell(returnStdout: true, script: '(az acr repository show-tags -n ' + REGISTRY_NAME + ' --repository ' + COMPOSE_PROJECT_NAME + '-solution --orderby time_desc --output tsv | Where-Object -FilterScript { $_ -match "' + params.Environment + '-*" })[0]')
                    VERSION = latest.trim()
                }
            }
        }
        
        stage('Deploy Images to AKS') {
            steps {
                script {
                    dir('custom-images') {
                        withKubeConfig(credentialsId: KUBERNETES_CREDENTIALS_ID, namespace: AKS_NAMESPACE, clusterName: AKS_CLUSTER_NAME, serverUrl: AKS_CLUSTER_API_URL) {
                            bat 'kubectl set image deployments/cm sitecore-xm1-cm=' + REGISTRY + COMPOSE_PROJECT_NAME + '-xm1-cm:' + VERSION + ' -n ' + AKS_NAMESPACE + ' --record'
                            bat 'kubectl set image deployments/cd sitecore-xm1-cd=' + REGISTRY + COMPOSE_PROJECT_NAME + '-xm1-cd:' + VERSION + ' -n ' + AKS_NAMESPACE + ' --record'
                        } 
                    }
                }
            }
        }
        
        stage('Wait for Deployment Rollout to Finish') {
            steps {
                script {
                    dir('custom-images') {
                        withKubeConfig(credentialsId: KUBERNETES_CREDENTIALS_ID, namespace: AKS_NAMESPACE, clusterName: AKS_CLUSTER_NAME, serverUrl: AKS_CLUSTER_API_URL) {
                            bat 'kubectl rollout status deployments/cm'
                            bat 'kubectl rollout status deployments/cd'
                        } 
                    }
                }
            }
        }
    }
}